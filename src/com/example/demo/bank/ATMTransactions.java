/**
 * 
 */
package com.example.demo.bank;

import com.example.demo.AccountApi;

/**
 * @author ADMIN
 *
 */
public class ATMTransactions {
	private String transactionId;
	private String date;
	private String type;
	private String amount;
	private String postBalance;
	private AccountApi account;

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the postBalance
	 */
	public String getPostBalance() {
		return postBalance;
	}

	/**
	 * @param postBalance the postBalance to set
	 */
	public void setPostBalance(String postBalance) {
		this.postBalance = postBalance;
	}

	/**
	 * @return the account
	 */
	public AccountApi getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(AccountApi account) {
		this.account = account;
	}

}
