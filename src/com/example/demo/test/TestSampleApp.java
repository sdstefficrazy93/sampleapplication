/**
 * 
 */
package com.example.demo.test;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.AccountApi;
import com.example.demo.CurrentAccount;
import com.example.demo.SavingsAccount;
import com.example.demo.bank.Customer;

/**
 * @author ADMIN
 *
 */
public class TestSampleApp {

	public static void main(String[] args) {
		Customer customer = new Customer();
		AccountApi savingsAccount = new SavingsAccount("SB1234516", "100000.0");
		AccountApi currentAccount = new CurrentAccount("154646", "50000");
		List<AccountApi> accounts = new ArrayList<>();

		savingsAccount.setNumber("SB1234516");
		savingsAccount.setBalance("100000.0");
		
		
		currentAccount.setNumber("154646");
		currentAccount.setBalance("50000");

		currentAccount.createTransactions("154646", "10000");
		System.out.println(currentAccount.getBalance());
		savingsAccount.createTransactions("1234516", "500000");
		System.out.println(savingsAccount.getBalance());
		accounts.add(savingsAccount);
		accounts.add(currentAccount);

		customer.setAccounts(accounts);
		customer.setName("Rajeev");
		customer.setCardNumber("4587-4569-7845-9963");
		System.out.println(customer.getAccounts());
		final double d1=1/2;
		System.out.println(d1);
		

	}
}
