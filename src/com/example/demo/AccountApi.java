/**
 * 
 */
package com.example.demo;

import com.example.demo.bank.ATMTransactions;

/**
 * @author ADMIN
 *
 */
public abstract class AccountApi {
	
	
	
	private ATMTransactions atmTransactions;

	private String number;

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the balance
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	private String balance;

	public String deposit(String number,String amount) {
		this.number = number;
		//this.balance = balance;
		
		
		
		
		String balance=getBalance();
		Double d=Double.valueOf(balance)+Double.valueOf(amount)	;	
	String	balanceNew=String.valueOf(d);
		this.setBalance(balanceNew);;
		return this.getBalance();
	}

	public String withdraw(String number, String balance,String withDrawAmount) {
		if(!number.isEmpty()&&withDrawAmount!=null&&balance!=null) {
			Double afterWithDrawalAmount=Double.valueOf(balance)-Double.valueOf(withDrawAmount);
			return String.valueOf(afterWithDrawalAmount);
		}
		
		
		
		return balance;

	}
	public void createTransactions(String number,String amount) {
		String balance=getBalance();
		
		if(!number.isEmpty()&&amount!=null&&Double.valueOf(amount)>0&&Double.valueOf(amount)<100000) {
			
			deposit(number, amount);
		}
		else if (Double.valueOf(balance)-Double.valueOf(amount)>0 &&Double.valueOf(amount)<100000) {
		String withdrawn=withdraw(number,balance,amount);
		setBalance(withdrawn);
		
		}
		
		Double transactionId=(Double.valueOf(amount)+Math.random());
		atmTransactions=new ATMTransactions();
		atmTransactions.setTransactionId(transactionId.toString());
}


	/**
	 * @return the atmTransactions
	 */
	public ATMTransactions getAtmTransactions() {
		return atmTransactions;
	}

	/**
	 * @param atmTransactions the atmTransactions to set
	 */
	public void setAtmTransactions(ATMTransactions atmTransactions) {
		this.atmTransactions = atmTransactions;
	}

}
