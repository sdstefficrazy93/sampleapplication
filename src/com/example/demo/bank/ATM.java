/**
 * 
 */
package com.example.demo.bank;

/**
 * @author ADMIN
 *
 */
public class ATM extends Bank {

	private String location;
	private String managedBy;

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the managedBy
	 */
	public String getManagedBy() {
		return managedBy;
	}

	/**
	 * @param managedBy the managedBy to set
	 */
	public void setManagedBy(String managedBy) {
		this.managedBy = managedBy;
	}

	public void identifies(String accountNumber, String pin) {

	}

	public void transactions(String balance, String accountNumber) {

	}

}
