/**
 * 
 */
package com.example.demo;

/**
 * @author ADMIN
 *
 */
public class CurrentAccount extends AccountApi {
	
	private SavingsAccount savingsAccount;
	private String accountNumber;
	private String balance;

	
	
	public CurrentAccount(String accountNumber, String balance) {
		//super();
		this.accountNumber = accountNumber;
		this.balance = balance;
		//this.currAccount = currAccount;
	}
	/**
	 * @return the savingsAccount
	 */
	public SavingsAccount getSavingsAccount() {
		return savingsAccount;
	}

	/**
	 * @param savingsAccount the savingsAccount to set
	 */
	public void setSavingsAccount(SavingsAccount savingsAccount) {
		this.savingsAccount = savingsAccount;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the balance
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	

	/**
	 * @param number
	 * @param balance
	 * @param withdrawAmount
	 * @return
	 */
	public String withdraw(String number, String balance, String withdrawAmount) {
if(Double.valueOf(balance)>Double.valueOf(withdrawAmount)) {
		Double d1 = Double.valueOf(balance);
		Double d2 = Double.valueOf(withdrawAmount);
		balance= String.valueOf(d1 - d2);
		return balance;
	}
return balance+ " Not sufficient funds to withdraw";
	}
	public String toString() {
		return "balance "+balance;
	}
	
}
