/**
 * 
 */
package com.example.demo.bank;

import com.example.demo.AccountApi;
import com.example.demo.CurrentAccount;
import com.example.demo.SavingsAccount;

/**
 * @author ADMIN
 *
 */
public abstract class Bank  extends AccountApi{
	
	private String code;
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	private String address;
	
	public AccountApi maintains(String accountNumber,String balance) {
		
		if(accountNumber.contains("SB")) {
			return new SavingsAccount(accountNumber, balance);	
		}
		return new CurrentAccount(accountNumber,balance);
		
	}

}
