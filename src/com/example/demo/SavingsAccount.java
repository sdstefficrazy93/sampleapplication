/**
 * 
 */
package com.example.demo;

/**
 * @author ADMIN
 *
 */

public class SavingsAccount extends AccountApi {
	private String accountNumber;
	private String balance;
	private CurrentAccount currAccount;

	/**
	 * @param accountNumber
	 * @param balance
	 * @param currAccount
	 */
	public SavingsAccount(String accountNumber, String balance) {
		//super();
		this.accountNumber = accountNumber;
		this.balance = balance;
		//this.currAccount = currAccount;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the balance
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	/**
	 * @return the currAccount
	 */
	public CurrentAccount getCurrAccount() {
		return currAccount;
	}

	/**
	 * @param currAccount the currAccount to set
	 */
	public void setCurrAccount(CurrentAccount currAccount) {
		this.currAccount = currAccount;
	}
	
	public String toString() {
		return "the balance is : "+balance +"the account number is "+ accountNumber;
	}
}
