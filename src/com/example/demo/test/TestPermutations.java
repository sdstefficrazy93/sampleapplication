/**
 * 
 */
package com.example.demo.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * @author ADMIN
 *
 */
public class TestPermutations {

	/**
	 * @param args
	 */
	public static <T extends Comparable<T>> void printAllOrdered(
			  String[] elements, char delimiter) {
			 
			    Arrays.sort(elements);
			    boolean hasNext = true;
			 
			    while(hasNext) {
			       
			    	
			    	
			    	printArray(elements, delimiter);
			        int k = 0, l = 0;
			        hasNext = false;
			        for (int i = elements.length - 1; i > 0; i--) {
			            if (elements[i].compareTo(elements[i - 1]) > 0) {
			                k = i - 1;
			                hasNext = true;
			                break;
			            }
			        }
			 
			        for (int i = elements.length - 1; i > k; i--) {
			            if (elements[i].compareTo(elements[k]) > 0) {
			                l = i;
			                break;
			            }
			        }
			 
			        swap(elements, k, l);
			        Collections.reverse(Arrays.asList(elements).subList(k + 1, elements.length));
			    }
			}
	private static void printArray(String[] input,char delimiter) {
	    System.out.print('\n');
	    for(int i = 0; i < input.length; i++) {
	        System.out.print(input[i]);
	    }
	}
	private static void swap(String[] input, int a, int b) {
	    String tmp = input[a];
	    input[a] = input[b];
	    input[b] = tmp;
	}
	public static void main(String[] args) {
		String arr[]={"Rajeev","Neha","Asur"};
		printAllOrdered(arr,',');
	}
}
